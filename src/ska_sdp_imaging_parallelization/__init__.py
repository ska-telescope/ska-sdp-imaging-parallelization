"""Top-level package for ska-sdp-imaging-parallelization."""

__author__ = """Prachi Dave"""
__email__ = "prachi.dave@skao.int"
__version__ = "0.0.1"

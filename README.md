# ska-sdp-imaging-parallelization

Scaling the SKA Science Data Processing calibration and imaging software application in Setonix


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-imaging-parallelization/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-imaging-parallelization/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-sdp-imaging-parallelization documentation](https://developer.skatelescope.org/projects/ska-sdp-imaging-parallelization/en/latest/index.html "SKA Developer Portal: ska-sdp-imaging-parallelization documentation")

## Features

* TODO

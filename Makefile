include .make/base.mk

############
## PYTHON ##
############
include .make/python.mk

PYTHON_LINE_LENGTH = 88
PYTHON_RUNNER = poetry run

##########
## DOCS ##
##########

DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create true
	poetry install --no-root --only docs

docs-do-build:
	poetry run python3 -m sphinx -b $(DOCS_TARGET_ARGS) $(DOCS_SOURCEDIR) $(DOCS_BUILDDIR) $(DOCS_SPHINXOPTS)

.PHONY: docs-pre-build

